import asyncio
import json

class SDPAnswerServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received SDP offer from', addr)
        print(message)
        answer = json.loads(message)
        # Cambiar los puertos en la respuesta
        for media in answer['media']:
            if media['type'] == 'audio':
                media['port'] = 34543
            elif media['type'] == 'video':
                media['port'] = 34543
        print('Sending SDP answer...')
        self.transport.sendto(json.dumps(answer).encode(), addr)

    def error_received(self, exc):
        print('Error received:', exc)

async def main():
    print("Starting SDP Answer Server")
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPAnswerServerProtocol(),
        local_addr=('127.0.0.1', 9999))
    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

asyncio.run(main())
