import asyncio
import json

class SDPJSONOfferClientProtocol:
    def __init__(self, on_con_lost):
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        sdp_data = {
            'version': 0,
            'origin': {'username': 'user', 'sessionId': 434344, 'sessionVersion': 0, 'netType': 'IN', 'ipVer': 4, 'address': '127.0.0.1'},
            'name': 'Session',
            'timing': {'start': 0, 'stop': 0},
            'connection': {'version': 4, 'ip': '127.0.0.1'},
            'media': [
                {'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000}, {'payload': 96, 'codec': 'opus', 'rate': 48000}],
                 'type': 'audio', 'port': 54400, 'protocol': 'RTP/SAVPF', 'payloads': '0 96', 'ptime': 20, 'direction': 'sendrecv'},
                {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000}, {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
                 'type': 'video', 'port': 55400, 'protocol': 'RTP/SAVPF', 'payloads': '97 98', 'direction': 'sendrecv'}
            ]
        }
        sdp_offer = {
            "type": "offer",
            "sdp": json.dumps(sdp_data)
        }
        print('Sending SDP offer as JSON...')
        self.transport.sendto(json.dumps(sdp_offer).encode())

    def datagram_received(self, data, addr):
        print("Received SDP answer as JSON:", data.decode())
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def main():
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPJSONOfferClientProtocol(on_con_lost),
        remote_addr=('127.0.0.1', 9999))
    try:
        await on_con_lost
    finally:
        transport.close()

asyncio.run(main())
