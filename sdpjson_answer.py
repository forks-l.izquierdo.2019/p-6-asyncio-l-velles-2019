import asyncio
import json


class SDPJSONAnswerServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received SDP offer as JSON from', addr)
        json_message = json.loads(message)
        sdp_data = json.loads(json_message["sdp"])
        # Cambiar los puertos en la respuesta
        for media in sdp_data['media']:
            if media['type'] == 'audio':
                media['port'] = 34543
            elif media['type'] == 'video':
                media['port'] = 34543
        sdp_answer = {
            "type": "answer",
            "sdp": json.dumps(sdp_data)
        }
        print('Sending SDP answer as JSON...')
        self.transport.sendto(json.dumps(sdp_answer).encode(), addr)

    def error_received(self, exc):
        print('Error received:', exc)


async def main():
    print("Starting SDP JSON Answer Server")
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SDPJSONAnswerServerProtocol(),
        local_addr=('127.0.0.1', 9999))
    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

asyncio.run(main())
